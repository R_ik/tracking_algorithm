import matplotlib
matplotlib.use("TkAgg")
from matplotlib import pyplot as plt
import tracking_algorithm
import numpy as np


USE_C_FUNCTIONS = True
##
FS = 100
NSAMPLES = 2000
NOISE_AMPLITUDE = 0.15

template = np.loadtxt("template_data.csv")
freq = np.linspace(0.2, 0.7, NSAMPLES)
phase = (np.cumsum(freq) % len(template)).astype(int)
epsilon = NOISE_AMPLITUDE*np.random.randn(NSAMPLES)
data = template[phase] + epsilon

plt.figure(1, clear=True)
phase_estimate = tracking_algorithm.asym_dtw_estimate_phase(template, data, backward=False, plot_enable=True, c_funcs=USE_C_FUNCTIONS)

plt.figure(2, clear=True)
plt.subplot(2,1,1)
plt.plot(np.arange(NSAMPLES)/FS, template[phase_estimate], label="$\\mathbf{y[\\theta_x]}$", alpha=1)
plt.plot(np.arange(NSAMPLES)/FS, data, label="$\\mathbf{x}$", ls='-')
plt.legend(loc="lower right")
plt.subplot(2, 1, 2)
plt.plot(np.arange(NSAMPLES)/FS, phase_estimate, label="$\\mathbf{\\theta}_{MLE}$", alpha=1)
plt.plot(np.arange(NSAMPLES)/FS, phase, label="actual $\\mathbf{\\theta}$", ls='-')
plt.legend(loc="lower right")

freq_backwards = np.linspace(0.7, -0.7, NSAMPLES)
phase_backwards = (np.cumsum(freq_backwards) % len(template)).astype(int)
epsilon = NOISE_AMPLITUDE*np.random.randn(NSAMPLES)
data_backwards = template[phase_backwards] + epsilon
plt.figure(3, clear=True)
phase_estimate_backwards = tracking_algorithm.asym_dtw_estimate_phase(template, data_backwards, backward=True, plot_enable=True, c_funcs=USE_C_FUNCTIONS)


linestyle_1 = '--'
linestyle_2 = '-'
plt.figure(4, clear=True, figsize=(6,5))
plt.subplot(2,1,1)
plt.plot(np.arange(NSAMPLES)/FS, data_backwards, label="$\\mathbf{x}$", ls=linestyle_2)
plt.plot(np.arange(NSAMPLES)/FS, template[phase_estimate_backwards], label="$\\mathbf{y[\\theta_x}]$", alpha=1, ls=linestyle_1)
plt.legend(loc="lower right")
plt.title("Signal and aligned template")
plt.subplot(2, 1, 2)
plt.plot(np.arange(NSAMPLES)/FS, phase_estimate_backwards / len(template) *2*np.pi, label="$\\mathbf{\\hat\\theta}_{x}$", alpha=1, ls=linestyle_2)
plt.plot(np.arange(NSAMPLES)/FS, phase_backwards / len(template) *2*np.pi, label="actual $\\mathbf{\\theta}$", ls=linestyle_1)
plt.legend(loc="lower right")
plt.ylabel("Phase (radians)")
plt.title("Phase and estimated phase")
plt.tight_layout()
plt.savefig("backwards_example.pdf")
##
import peakutils
def heuristic_peak_detect(data, threshold, min_spacing):
    peak_indices = peakutils.indexes(data, thres=threshold, min_dist=min_spacing)
    heuristic_phase_increments = np.zeros(NSAMPLES)
    for peak_index in peak_indices:
        heuristic_phase_increments[peak_index] += FS
    heuristic_phase_estimate = np.cumsum(heuristic_phase_increments)
    return heuristic_phase_estimate


NOISE_AMPLITUDE = 0.55
template = np.loadtxt("template_data.csv")
rmse = lambda phase, estimate_phase: np.sqrt(np.mean((phase-estimate_phase)**2))

errors_dtw = []
errors_peaks = []
for i in range(1000):
    print(i)
    freq = np.linspace(0.2, 0.7, NSAMPLES)
    phase = (np.cumsum(freq) % len(template)).astype(int)
    epsilon = NOISE_AMPLITUDE * np.random.randn(NSAMPLES)
    data = template[phase] + epsilon

    full_phase = tracking_algorithm.recover_full_phase(phase, FS)
    phase_estimate = tracking_algorithm.asym_dtw_estimate_phase(template, data, backward=False, plot_enable=False,
                                                                c_funcs=USE_C_FUNCTIONS)

    full_estimated_phase = tracking_algorithm.recover_full_phase(phase_estimate, FS) - np.round(phase_estimate[0] / FS)*FS

    heuristic_phase_estimate = heuristic_peak_detect(data, 0.8, 140)

    rmse_dtw = rmse(full_phase, full_estimated_phase)
    rmse_peaks = rmse(full_phase, heuristic_phase_estimate)

    errors_dtw.append(rmse_dtw)
    errors_peaks.append(rmse_peaks)

##
plt.figure(5, clear=True, figsize=(6,5))


plt.subplot(2, 1, 1)

plt.plot(np.arange(NSAMPLES)/FS, data)
plt.title("Recorded signal")
plt.ylabel("Signal value")
ax = plt.gca()

plt.subplot(2, 1, 2, sharex=ax)

plt.title("Unwrapped phase and estimates")
plt.plot(np.arange(NSAMPLES)/FS, full_phase / len(template) *2*np.pi, label="$\\mathbf{\\theta}_{x}$")
plt.plot(np.arange(NSAMPLES)/FS, full_estimated_phase / len(template) *2*np.pi, label="$\\mathbf{\\hat\\theta}_{x}$", ls = linestyle_1)
plt.plot(np.arange(NSAMPLES)/FS, heuristic_phase_estimate / len(template) *2*np.pi, label="$\\mathbf{\\hat\\theta}_{peaks}$")
plt.legend()
plt.ylabel("Unwrapped phase (radians)")
plt.xlim(0, NSAMPLES/FS)

print(np.mean(errors_dtw))
print(np.mean(errors_peaks))

plt.tight_layout()
plt.savefig("comparison_peakdetect.pdf")


##

