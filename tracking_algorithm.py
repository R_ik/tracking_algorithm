import matplotlib.pyplot as plt
import numpy as np
import tracking_algorithm_c_wrappers

CDLL_PATH = "./asym_dtw.so"

def asym_dtw(template, data, init_cost = None, backward=True):
    M = len(template)
    N = len(data)
    D = np.zeros((N,M))
    # initialize the first row with the local cost
    if init_cost is None:
        D[0,:] = np.square(template-data[0])
    else:
        D[0,:] = init_cost
    # repeat template twice so that the first value is returned for template[M+1]
    template = np.tile(template, 2)
    # moves = np.array([-1, 0, 1])
    if backward:
        for i in range(1,N):
            for j in range(M):
                D[i,j] = min(D[i-1, (j-1)%M], D[i-1, j], D[i-1, (j+1)%M]) + (template[j]-data[i])**2
    else:
       for i in range(1,N):
            for j in range(M):
                D[i,j] = min(D[i-1, (j-1)%M], D[i-1, j]) + (template[j]-data[i])**2
    return D


def backtrace(D, backward=True):
    N,M = D.shape
    theta_x = np.zeros(N, dtype=np.int)
    j = np.argmin(D[-1,:])
    for i in range(N-1, -1, -1):
        # find possible predecessors of this node
        theta_x[i] = j
        if backward:
            j_prevs = np.arange(j-1,j+2) % M
            j = j + (np.argmin(D[i-1,j_prevs])) - 1
        else:
            j_prevs = np.arange(j-1,j+1) % M
            j = j + np.argmin(D[i-1,j_prevs]) - 1
        # find index of lowest cost predecessor
        j %= M
        # add to phase function
    return theta_x

# def orig_dtw_estimate_phase(template, data):
#     D = dtw_c_wrapper(template, data)
#     N,M = D.shape
#     min_i = np.argmin(D[:,-1])
#     min_j = np.argmin(D[-1, :])
#     if D[min_i, -1] < D[-1, min_j]:
#         i_K, j_K = (min_i, M-1)
#     else:
#         i_K, j_K = (N-1, min_j)
#
#     theta_x = backtrace_orig_dtw_c_wrapper(D, i_K, j_K)
#
#     return theta_x, D, (i_K, j_K)


def asym_dtw_estimate_phase(template, data, init_cost = None, backward=True, plot_enable=False, c_funcs = True, return_cost_mat = False):
    if c_funcs:
        cfunctions = tracking_algorithm_c_wrappers.CFunctionsHandler(CDLL_PATH)
        D = cfunctions.asym_dtw_c_wrapper(template, data, init_cost, backward)
    else:
        D = asym_dtw(template, data, init_cost, backward)
    if plot_enable:
        plt.imshow((D.T), aspect='auto')
    if c_funcs:
        theta_x = cfunctions.backtrace_c_wrapper(D, backward)
    else:
        theta_x = backtrace(D, backward)
    if plot_enable:
        plt.plot(theta_x)
        plt.gca().invert_yaxis()
        pass
    if return_cost_mat:
        return theta_x, D
    else:
        return theta_x

def align_template(template, phase_estimate):
    return template[phase_estimate]

def asym_dtw_nohist(template, data):
    M = len(template)
    N = len(data)
    local_estimate = []
    # initialize the prob row with the local cost
    D = np.square(template - data[0])
    # repeat template twice so that the first value is returned for template[M+1]
    template = np.tile(template, 2)
    for i in range(1, N):
        for j in range(M):
            j_prevs = np.arange(j - 1, j + 2) % M
            D[j] = np.min(D[j_prevs]) + (template[j] - data[i]) ** 2
        local_estimate.append(np.argmin(D))
    return local_estimate

def recover_full_phase(theta_x, temp_len):
    theta_x_abs = np.zeros(theta_x.shape)
    last = theta_x[0]
    offset_count = 0
    for i, x in enumerate(theta_x):
        if last - x > temp_len //2:
            offset_count += 1
        if x - last > temp_len // 2:
            offset_count -= 1
        theta_x_abs[i] = x+temp_len*offset_count
        last = x
    return theta_x_abs
