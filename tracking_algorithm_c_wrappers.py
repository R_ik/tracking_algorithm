import numpy as np
import ctypes
from numpy.ctypeslib import ndpointer


class CFunctionsHandler:
    def __init__(self, CDLL_path):
        try:
            asym_dtw_dll = ctypes.CDLL(CDLL_path)

            self.asym_dtw_c = asym_dtw_dll.asym_dtw
            self.asym_dtw_c.argtypes = [ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
                                   ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
                                   ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
                                   ctypes.c_int,
                                   ctypes.c_int,
                                   ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
                                   ctypes.c_size_t,
                                   ctypes.c_size_t]

            self.backtrace_c = asym_dtw_dll.backtrace
            self.backtrace_c.argtypes = [ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
                                   ndpointer(ctypes.c_int, flags="C_CONTIGUOUS"),
                                    ctypes.c_int,
                                    ctypes.c_int,
                                    ctypes.c_int]

            self.backtrace_orig_dtw = asym_dtw_dll.backtrace_orig_dtw
            self.backtrace_orig_dtw.argtypes = [ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
                                   ndpointer(ctypes.c_int, flags="C_CONTIGUOUS"),
                                    ctypes.c_int,
                                    ctypes.c_int,
                                    ctypes.c_int]

            self.to_absolute_phase_c = asym_dtw_dll.to_absolute_phase
            self.to_absolute_phase_c.argtypes = [ndpointer(ctypes.c_int, flags="C_CONTIGUOUS"),
                                   ndpointer(ctypes.c_int, flags="C_CONTIGUOUS"),
                                    ctypes.c_int,
                                    ctypes.c_int]

            self.dtw_c = asym_dtw_dll.orig_dtw
            self.dtw_c.argtypes = [ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
                                   ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
                                   ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
                                   ctypes.c_size_t,
                                   ctypes.c_size_t]
        except OSError:
            print("CDLL file not found or not readable")
            raise

    def asym_dtw_c_wrapper(self, template, data, init_cost = None, backward=True):
        y = template.astype('float64')
        x = data.astype('float64')
        if init_cost is None:
            init_cost_bool = 0
            init_cost = np.empty(0, dtype=np.float64)
        else:
            init_cost_bool = 1
        D = np.empty((len(x), len(y)), dtype=np.float64)
        self.asym_dtw_c(y, x, D, backward, init_cost_bool, init_cost, len(x), len(y))
        return D

    def dtw_c_wrapper(self, template, data):
        y = template.astype('float64')
        x = data.astype('float64')
        D = np.empty((len(x), len(y)), dtype=np.float64)
        self.dtw_c(y, x, D, len(x), len(y))
        return D

    def backtrace_c_wrapper(self, D, backward=True):
        N, M = D.shape
        D = D.astype(np.float64)
        theta_x = np.zeros(N, dtype=np.int32)
        self.backtrace_c(D, theta_x, backward, N, M)
        return theta_x

    def backtrace_orig_dtw_c_wrapper(self, D, i_K, j_K):
        D = D.astype(np.float64)
        theta_x = np.zeros(D.shape[0], dtype=np.int32)
        self.backtrace_orig_dtw(D, theta_x, D.shape[1], i_K, j_K)
        return theta_x

    def to_absolute_phase_c_wrapper(self, phase_estimate, templ_len):
        absolute_phase = np.zeros(phase_estimate.shape, dtype=ctypes.c_int)
        self.to_absolute_phase_c(phase_estimate.astype(np.int32), absolute_phase, len(phase_estimate), templ_len)
        return absolute_phase
