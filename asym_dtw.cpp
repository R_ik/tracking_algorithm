// C implementation of the core functions of the core of the Dynamic Time Warping algorithm
// Author: Rik van der Vlist
// Date: 2018-08-16

#include <stdio.h>

extern "C"
{   // declare function specifications for the DLL
	__declspec(dllexport) void asym_dtw(double*, double*, double*, int, int, double*, size_t, size_t);
	__declspec(dllexport) void backtrace(double*, int*, int, int, int);
	__declspec(dllexport) void to_absolute_phase(int* phase_estimate, int* absolute_phase, int N, int max_val);
	__declspec(dllexport) void orig_dtw(double* templ, double* data, double* D, size_t N, size_t M);
	__declspec(dllexport) void backtrace_orig_dtw(double* D, int* theta_x, int M, int i_K, int j_K);
}


double min_of_three(double a, double b, double c){
	double m = a;
	if (m > b) m = b;
	if (m > c) m = c;
	return m;
}

int argmin(double a, double b, double c){
	int i = 0;
	double m = a;
	if (m > b){
		m = b;
		i = 1;
	}
	if (m > c){
		m = c;
		i = 2;
	}
	return i;
}

void asym_dtw(double* templ, double* data, double* D, int backwards, int init_cost_bool, double* init_cost, size_t N, size_t M){
	double cost;
	float min_val;
	if(init_cost_bool){
		for(int j = 0; j < M; j++){
			D[j] = init_cost[j];
		}
	} else {
		for(int j = 0; j < M; j++){
			D[j] = (templ[j]-data[0])*(templ[j]-data[0]);
		}
	}
	if (backwards){
		for(int i = 1; i < N; i++){
			for(int j = 0; j < M; j++){
				cost = (templ[j]-data[i])*(templ[j]-data[i]);
				D[i*M+j] = min_of_three(D[(i-1)*M + (j+M-1)%M], D[(i-1)*M + j], D[(i-1)*M + (j+1)%M]) + cost;
			}
		}
	} else {
		for(int i = 1; i < N; i++){
			for(int j = 0; j < M; j++){
				cost = (templ[j]-data[i])*(templ[j]-data[i]);
				min_val = D[(i-1)*M + (j+M-1)%M];
				if (min_val > D[(i-1)*M + j]) min_val = D[(i-1)*M + j];
				D[i*M+j] = min_val + cost;
			}
		}
	}
}

void to_absolute_phase(int* phase_estimate, int* absolute_phase, int N, int max_val){
	int threshold = max_val / 2;
	int offset = 0;
	int last_val = phase_estimate[0];

	absolute_phase[0] = phase_estimate[0];
	for(int i = 1; i < N; i++){
		if(phase_estimate[i] < last_val - threshold)
			offset += max_val;
		if(phase_estimate[i] > last_val + threshold)
			offset -= max_val;
		absolute_phase[i] = phase_estimate[i] + offset;
		last_val = phase_estimate[i];
	}
}

void orig_dtw(double* templ, double* data, double* D, size_t N, size_t M){
    double cost;
    double left, left_down, down;
    // initialize corner
    D[0] = (templ[0]-data[0])*(templ[0]-data[0]);

    // initialize bottom row
    for(int i = 1; i < N; i++){
        D[i*M] = (templ[0]-data[i])*(templ[0]-data[i]) + D[(i-1)*M];
    }
    // initialize first column
    for(int j = 1; j < M; j++){
        D[j] = (templ[j]-data[0])*(templ[j]-data[0]) + D[j-1];
    }

    // Compute cumulative cost matrix
    for(int i = 1; i < N; i++){
        for(int j = 1; j < M; j++){
            cost = (templ[j]-data[i])*(templ[j]-data[i]);
            D[i*M+j] = min_of_three(D[(i-1)*M + j-1] + 2*cost, D[(i-1)*M + j] + cost, D[i*M + j-1] + cost);
        }
    }
}

void backtrace_orig_dtw(double* D, int* theta_x, int M, int i_K, int j_K){
	double minval = 1e16;
	double cost_left, cost_down, cost_left_down;
    int i = i_K, j = j_K;
    int step;

    while (i > 0 && j > 0){
        // update the phase function
        theta_x[i] = j;
        // compute cost off all predecessor.
        // if outside bounds set cost to 1e100
        if(i > 0)
            cost_left = D[(i-1)*M+j];
        else
            cost_left = 1e100;

        if(j > 0)
            cost_down = D[i*M + j-1];
        else
            cost_down = 1e100;

        if(i > 0 && j > 0)
            cost_left_down = D[(i-1)*M + j-1];
        else
            cost_left_down = 1e100;
        // find best predecessor
        step = argmin(cost_left, cost_down, cost_left_down);
        // decrease the coordinates
        switch(step){
        case 0:
            i -= 1;
            break;
        case 1:
            j -= 1;
            break;
        case 2:
            i -= 1;
            j -= 1;
        }
    }
}

void backtrace(double* D, int* theta_x, int backwards, int N, int M){
	double minval = 1e16;
	int min_idx = 0;
	int offset = (N-1)*M;
	int curr_j;
	int abs_phase;
	int step;
	// initialize the algorithm at lowest value of last row
	for(int j = 0; j < M; j++){
		if(D[offset+j] < minval){
			minval = D[offset+j];
			min_idx = j;
		}
	}

	// set starting state to minimum index found
	curr_j = min_idx;
	if(backwards){
		for(int i = N-1; i > 0; i--){
			theta_x[i] = curr_j;
			// find the minimal cost of possible predecessors
			step = argmin(D[(i-1)*M+(curr_j+M-1)%M], D[(i-1)*M+curr_j], D[(i-1)*M+(curr_j+1)%M]);
			abs_phase = abs_phase + step - 1;
			curr_j = (curr_j + step - 1 + M) % M;
		}
		theta_x[0] = curr_j;
	}

	else {
		for(int i = N-1; i > 0; i--){
			theta_x[i] = curr_j;
			// argmin of two options reduces to shorthand if statement
			step = (D[(i-1)*M+(curr_j+M-1)%M] < D[(i-1)*M+curr_j]) ? 0 : 1;
			curr_j = (curr_j + step - 1 + M) % M;
		}
		theta_x[0] = curr_j;
	}

}
