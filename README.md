# Algorithm for tracking repeating patterns in time series
*Written by Rik van der Vlist, 27-02-2019*

## Requirements
Install `numpy`, `matplotlib` and `peakutils` using the `pip install` command.

## Usage
An example of the usage is found in the file `algorithm_demonstration.py`. 
The file reads a template from a CSV file. Next, a signal is generated based on the signal model, and noise is added. 
The noisy signal along with the template is then fed to the tracking algorithm. 

## C implementation
The algorithm is implemented both in Python and in C. The compiled C code can be called as a Python module and speeds the code up largely for larger datasets. 
First, compile the C code into a shared library:
For Linux

`$  gcc -shared -Wl,-soname,asym_dtw -o asym_dtw.so -fPIC -fdeclspec asym_dtw.cpp`

For Mac

`$ gcc -shared -Wl,-install_name,asym_dtw.so -o asym_dtw.so -fPIC -fdeclspec asym_dtw.cpp`

For Windows, the code can be compiled into a DLL file, but this is slightly more complicated. 
To use the C functions, set the `USE_C_FUNCTIONS` flag in the demonstration script, and set the `CDLL_PATH` in `tracking_algorithm.py` to point to the library file that was just compiled. 